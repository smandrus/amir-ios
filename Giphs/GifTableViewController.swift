//
//  GifTableViewController.swift
//  Giphs
//
//  Created by A S on 6/19/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON
import SQLite

class GifTableViewController: UITableViewController, UITextFieldDelegate{
    //MARK: Properties
    let key: String = "dc6zaTOxFJmzC"
    let placeholderImage: UIImage = UIImage(named:"loadingb")!
    // let ROULETTE_URL = "http://api.giphy.com/v1/gifs/random"
    let GIFS_TO_LOAD: Int = 30
    let DB_MANAGER:DBmanager = DBmanager()
    let CM: ColourMaker = ColourMaker()
    let EVEN_COLOUR = "913bff"
    let ODD_COLOUR = "f66866"
    
    var gifs = [Gif]()
    var list = [Gif]()
    var searchQ: String = ""
    var swiftyj: JSON = []
    var favedIds: [String] = []
    
    @IBOutlet weak var searchBox: UITextField!
    @IBOutlet weak var favesBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFeed()
        self.favedIds = DB_MANAGER.getIds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.favedIds = DB_MANAGER.getIds()
        searchBox.text = ""
        gifs.removeAll()
        loadFeed()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return gifs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "GifTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? GifTableViewCell
        
        if indexPath.row < gifs.count {
            
            let gif = gifs[indexPath.row]
            if indexPath.row % 2 == 0 {
                cell?.backgroundColor = CM.makeUIColor(hex: EVEN_COLOUR)
            } else {
                cell?.backgroundColor = CM.makeUIColor(hex: ODD_COLOUR)
            }
            
            // pass in gif object
            cell?.setGif(gif: gif)
            
            // check if image is in favourites
            if self.favedIds.contains(gif.id){
                cell?.faveBtn.setImage(#imageLiteral(resourceName: "faveon"), for: .normal)
                cell?.pressed =  true
            } else {
                cell?.faveBtn.setImage(#imageLiteral(resourceName: "faveoff"), for: .normal)
                cell?.pressed =  false
            }
            
            // add loading activity indicator from King Fisher
            //cell?.imageView?.kf.indicatorType = .activity
            cell?.ActivityIndicator.startAnimating()
            // get image
            cell?.imageView?.kf.setImage(with: gif.url, placeholder: placeholderImage, completionHandler: {
                (image, error, cacheType, imageUrl) in
                cell?.ActivityIndicator.stopAnimating()
            })
        }
        return cell!
    }
    
    //MARK: Private Methods
    
    /* makes a get request to the url given. sets json response in swiftyj */
    func alamo (url: String) {
        func getRequest(completionHandler: @escaping (JSON?) -> ()) {
            Alamofire.request(url, headers: ["api_key": self.key]).responseJSON() { response in
                if((response.result.value) != nil) {
                    completionHandler(JSON(response.result.value!))
                } else {
                    completionHandler(nil)
                }
            }
        }
        getRequest() { json in
            if (json != nil){
                self.swiftyj = json!
                self.extractData()
                self.gifs = self.list
                self.list.removeAll()
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
    func extractData() {
        // Get data
        if let temp = self.swiftyj["data"].array {
            // Loop through each gif result
            for x in temp {
                // add gif object to list of gifs
                self.list.append(makeGifObjectFromJson(jsonObj: x))
            }
        } else {
            print ("error in function: extractData\(self.swiftyj["data"])")
        }
    }
    
    /* returns gif object from json */
    func makeGifObjectFromJson (jsonObj: JSON) -> Gif{
        var fw  = URL(string: "")
        var fw_s = URL(string: "")
        
        // grab id
        let id = jsonObj["id"].stringValue
        
        // grab links for fixed width  and fixed width small gifs
        for (key, value) in jsonObj["images"] {
            switch key {
            case "fixed_width" :
                fw = URL(string: value["url"].stringValue)!
            case "fixed_width_small" :
                if (URL(string: value["url"].stringValue) != nil) {
                    fw_s = URL(string: value["url"].stringValue)!
                } else {
                    fw_s = fw
                }
            default:
                continue
            }
        }
        
        return Gif(id: id, url: fw!, url_s: fw_s!)
    }
    
    /* search for gifs. */
    func searchString(text: String) {
        var searchString = text.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
        searchString = searchString.replacingOccurrences(of: " ", with: "+")
        
        if searchString != "" {
            let url = "http://api.giphy.com/v1/gifs/search?q=\(searchString)"
            
            // empty current list
            self.list.removeAll()
            
            // make request with alamofire and load table
            alamo(url: url)
        } else {
            loadFeed()
        }
    }
    
    func loadFeed(){
        let url = "http://api.giphy.com/v1/gifs/trending"
        alamo(url: url)
    }
    @IBAction func updateSearchQuery(_ sender: UITextField) {
        gifs.removeAll()
        searchString(text: searchBox.text!)
    }
    
    @IBAction func beginSearch(_ sender: Any) {
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    @IBAction func clearSearch(_ sender: Any) {
        searchBox.text = ""
        gifs.removeAll()
        loadFeed()
    }
}
