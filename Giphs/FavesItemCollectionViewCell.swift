//
//  FavesItemCollectionViewCell.swift
//  Giphs
//
//  Created by A S on 6/29/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class FavesItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var faveObj: Favourite!
    
    func setFaveItem(_ favourite: Favourite) {
        self.faveObj = favourite
        imageView.image = UIImage.gif(data: favourite.data)
    }
}
