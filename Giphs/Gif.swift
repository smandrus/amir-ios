//
//  Gif.swift
//  Giphs
//
//  Created by A S on 6/19/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import Foundation

class Gif {
    var id: String = ""
    var url: URL
    var url_s: URL
    
    
    init (id:String, url: URL, url_s: URL) {
        self.id = id
        self.url = url
        self.url_s = url_s
    }
}
