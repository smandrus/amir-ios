//
//  DBmanager.swift
//  Giphs
//
//  Created by A S on 6/28/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import Foundation
import Kingfisher
import SQLite
import SwiftGifOrigin

class DBmanager {
    var db = try? Connection("")
    let favesTable = Table("faves")
    
    let gifid = Expression<String>("gifid")
    let data = Expression<Data>("data")
    
    init() {
        // open db connection
        self.db = openConnection() as? Connection
        
        // dropFavesTable()
        // create table
        createFavesTable()
    }
    
    func openConnection() -> Any {
        let PATH = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        // open db connection
        do {
            let db1 = try? Connection("\(PATH)/db.sqlite3")
            return db1!
        } catch {
            print ("DB connection error in DBmanager\n")
        }
    }
    
    func createFavesTable() {
        do {
            try self.db?.run(self.favesTable.create(ifNotExists: true) { t in
                t.column(self.gifid, primaryKey: true)
                t.column(self.data, unique: false)
            })
            
        } catch {
            print ("Could not create table\n")
        }
    }
    
    func insert(gif: Gif, imageDL: Image) {
        createFavesTable()
        
        let data0 = imageDL.kf.gifRepresentation()
        
        do {
            let insert = self.favesTable.insert(self.gifid <- gif.id, self.data <- data0!)
            _ = try db?.run(insert)
        } catch {
            print("insertion failed: \(error)")
        }
    }
    
    func delete (id: String) {
        let temp = favesTable.filter(gifid == id)
        do {
            try db?.run(temp.delete())
            print ("Deleted \(id)\n")
        } catch {
            print ("Could not delete \(id)\n")
        }
    }
    
    func dropFavesTable() {
        do {
            try db?.run(self.favesTable.drop())
        } catch {
            print ("could not drop table \n")
        }
    }
    
    func getAll() -> [Favourite] {
        var favourites = [Favourite]()
        do {
            let dbConnection = openConnection()
            for x in try (dbConnection as! Connection).prepare(self.favesTable) {
                favourites.append(Favourite(id: x[self.gifid], data: x[self.data]))
            }
            return favourites
        } catch {
            print ("*** could not get all \n")
        }
        return favourites
    }
    
    func getIds() -> [String] {
        var r = [String]()
        do {
            let dbConnection = openConnection()
            for x in try (dbConnection as! Connection).prepare(self.favesTable) {
                r.append(x[self.gifid])
            }
            return r
        } catch {
            print ("*** could not get ids \n")
        }
        return r
    }
    
    func getImageWith(id: String) -> UIImage{
        do {
            let dbConnection = openConnection()
            for x in try (dbConnection as! Connection).prepare(self.favesTable) {
                if (x[self.gifid] == id) {
                    return UIImage.gif(data: x[self.data])!
                }
            }
        } catch {
            print ("*** could not get image with id \n")
        }
        return UIImage(named: "loadingb")!
    }
}
