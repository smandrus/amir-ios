//
//  FavesViewController.swift
//  Giphs
//
//  Created by A S on 6/29/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import UIKit
import SwiftGifOrigin

private let reuseIdentifier = "FavesItemCollectionViewCell"

class FavesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var deleteBtn: UIBarButtonItem!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    
    let SELECTED_COLOUR: String = "59f796"
    let BG_COLOUR: String = "f66866"
    let CM: ColourMaker = ColourMaker()
    
    var favourites: [Favourite] = []
    var selectedIndexPath: IndexPath!{
        didSet{
            updateDeleteBtnState()
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        collectionView.backgroundColor = CM.makeUIColor(hex: BG_COLOUR)
        doneBtn.isEnabled = false
        deleteBtn.isEnabled = false
        super.viewDidLoad()
        print ("loaded favesView\n")
        initFavouriteItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        doneBtn.isEnabled = false
        deleteBtn.isEnabled = false
        super.viewWillAppear(animated)
        initFavouriteItems()
    }
    
    fileprivate func initFavouriteItems() {
        let dbMan = DBmanager()
        self.favourites = dbMan.getAll()
        print ("Favourites has \(self.favourites.count) items")
        collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return favourites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)as! FavesItemCollectionViewCell
        
        // Configure the cell
        cell.setFaveItem(favourites[indexPath.row])
        
        var borderColor: CGColor! = UIColor.clear.cgColor
        var borderWidth: CGFloat = 0
        if (selectedIndexPath != nil) {
            if indexPath == selectedIndexPath as IndexPath{
                borderColor = self.CM.makeUIColor(hex: SELECTED_COLOUR).cgColor
                borderWidth = 3
            }else{
                borderColor = UIColor.clear.cgColor
                borderWidth = 0
            }
        }
        cell.imageView.layer.borderWidth = borderWidth
        cell.imageView.layer.borderColor = borderColor
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    /*
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
     }
     */
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
    }
    
    // MARK: - UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = favourites[indexPath.row].getImage().size
        //let picDimension = self.view.frame.size.width / 4.0
        return CGSize(width: size.width, height: size.height)
    }
    
    @IBAction func deleteSelected(_ sender: Any) {
        let dbMan = DBmanager()
        //dbMan.dropFavesTable()
        dbMan.delete(id: favourites[selectedIndexPath.row].id)
        selectedIndexPath = nil
        initFavouriteItems()
    }
   

    @IBAction func doneSelecting(_ sender: Any) {
        selectedIndexPath = nil
        updateDeleteBtnState()
        collectionView.reloadData()

    }
    
    func makeUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func updateDeleteBtnState() {
        if (selectedIndexPath != nil) {
            deleteBtn.isEnabled = true
            doneBtn.isEnabled = true
        } else {
            deleteBtn.isEnabled = false
            doneBtn.isEnabled = false
        }
    }
    
}
