//
//  Favourite.swift
//  Giphs
//
//  Created by A S on 6/29/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import Foundation
import Kingfisher

class Favourite {
    var data: Data
    var id: String = ""
    
    init (id: String, data: Data) {
        self.id = id
        self.data = data
    }
    
    func getImage() -> Image {
        return Image(data: self.data)!
    }
}
