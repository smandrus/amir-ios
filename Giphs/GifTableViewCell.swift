//
//  GifTableViewCell.swift
//  Giphs
//
//  Created by A S on 6/19/17.
//  Copyright © 2017 ca.shayegh. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftGifOrigin

class GifTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var faveBtn: UIButton!
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
    var pressed: Bool = false
    var gif: Gif!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    func setGif(gif: Gif) {
        self.gif = gif
    }
    
    @IBAction func share(_ sender: Any) {
        do {
            let data: NSData = try NSData(contentsOf: gif.url)
        //let image : UIImage = UIImage.gif(data: data!)!
        //let link = self.gif.url
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [data], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo,
            UIActivityType.copyToPasteboard,
            UIActivityType.postToFacebook,
            UIActivityType.postToTwitter,
        ]
        
         self.window?.rootViewController!.present(activityViewController, animated: true, completion: nil)
        } catch {
            print ("Could not share \n")
        }
    }
   
    @IBAction func faveClicked(_ sender: UIButton) {
        
        if (pressed) {
            sender.setImage(#imageLiteral(resourceName: "faveoff"), for: .normal)
            pressed =  false
            // remove from favourites
            deleteGifWith(id: gif.id)
        } else {
            sender.setImage(#imageLiteral(resourceName: "faveon"), for: .normal)
            pressed =  true
            
            // add to favourites:
            
            // get image
            KingfisherManager.shared.retrieveImage(with: self.gif.url_s, options: nil, progressBlock: nil) {
                (image, error, cacheType, url) in
                if let image = image {
                    // insert into database
                    self.save(image: image)
                }
            }
        }
 
    }
    
    func save(image: Image) {
        let dbman = DBmanager()
        dbman.insert(gif: self.gif, imageDL: image)
    }
    
    func deleteGifWith(id: String) {
        let dbman = DBmanager()
        dbman.delete(id: id)
    }
    

}
